@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <p>
            A resistência utilizada apresentou o valor de 23,7 ohms.
        </p>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Imagem</th>
                    <th>Fonte Analisada</th>
                    <th>Tensão Nominal</th>
                    <th>Capacidade de Corrente</th>
                    <th>Valor da tensão interna (E)</th>
                    <th>Queda de tensão interna (V) </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($medicoes as $medicao)
                    <tr>
                        <td>{{$medicao->pilha_bateria}}</td>
                        <td>{{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                        <td>{{$medicao->capacidade_corrente}}</td>
                        <td>{{$medicao->tensao_sem_carga}}</td>
                        <td>{{$medicao->tensao_com_carga}}</td>
                        <td>{{$medicao->resistencia_carga}}</td>
                        <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </main>
@endsection

@section('rodape')
    <h4>Rodapé da página medições</h4>
@endsection