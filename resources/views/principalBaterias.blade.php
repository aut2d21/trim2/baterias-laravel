@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma 2D2 - Grupo 3</h1>
        <h2>Participantes:</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072542</td>
                <td>Ana Clara Alves Belmonte Galvão</td>
                <td>Gerente</td>
            </tr>
            <tr>
                <td>0072555</td>
                <td>Isabella Heloísa J. do Nascimento</td>
                <td>Desenvolvedor HTML, CSS e Java</td>
            </tr>
            <tr>
                <td>0072545</td>
                <td>Maria Eduarda Neves de Almeida</td>
                <td>Medições, dados e informações</td>
            </tr>
            <tr>
                <td>0073325</td>
                <td>Rayssa de Oliveira Mendes</td>
                <td>Medições, dados e informações</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/mira.png" alt="componentes do grupo">
    </main>

@endsection 

@section('rodape')
    <h4>Rodapé da página principal<h4>
@endsection