@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Teoria</h1>
        <hr>

        <h2>Baterias: o que são?</h2>
        <img class="bateria" src="../imgs/bateria.png" alt="bateria">
        <p>
            As baterias são conjuntos de pilhas ligadas em série, ou seja, são dispositivos eletroquímicos nos quais ocorrem reações de oxidorredução, produzindo uma corrente elétrica. Podem ser chamadas ainda de pilhas secundárias, baterias secundárias ou acumuladores.
        </p>
        <p>
            Baterias ou acumuladores são dispositivos que conseguem produzir e armazenar uma certa quantidade de energia por meio dos processos de oxidação e redução. De uma forma geral, trata-se de um conjunto de pilhas associadas em série, em que o polo positivo de uma está ligado ao polo negativo da outra. Todavia, vale ressaltar que nem sempre elas seguem esse padrão.
        </p>
        <p>
            As baterias funcionam como uma pilha convencional, ou seja, dentro do dispositivo ocorrem uma reação de oxidação e outra de redução, o que gera produção de corrente elétrica. Sendo assim, à medida que o dispositivo vai sendo utilizado, a quantidade do material que sofre oxidação (redutor) vai diminuindo. Quando a quantidade do redutor chega ao fim, o dispositivo para de gerar corrente elétrica (está descarregado).
        </p>
        <p>
            Ao conectarmos a bateria ou acumulador a um fonte elétrica externa, a corrente elétrica faz com que a reação de oxidação e redução torne-se reversível. Dessa forma, os componentes do redutor voltam a ser originados. Quando a quantidade do redutor retorna totalmente à quantidade anterior, dizemos que a bateria foi recarregada.
        </p>
        <p>
            Existem três tipos de baterias muito utilizadas nos dias atuais: Baterias ou acumuladores de chumbo, Bateria de Níquel-Cádmio e Bateria de ións Lítio.
        </p>

        <h3>O que são pilhas?</h3>
        <img class="pilha" src="../imgs/pilha.png" alt="pilha">
        <p>
            A pilha é um sistema onde ocorre a reação de oxirredução. Nesse dispositivo, a energia química produzida na reação espontânea é convertida em energia elétrica.
        </p>
        <p>
            As reações de oxidação e redução ocorrem simultaneamente em uma pilha. Quando uma espécie sofre oxidação ela doa elétrons para a outra espécie que, ao recebê-los, sofre redução.
        </p>
        <p>
            Portanto, quem sofre oxidação é o agente redutor e quem sofre redução é o agente oxidante.
        </p>
        <p>
            A oxidação ocorre quando uma espécie perde elétrons e se transforma em um cátion: A → A+ + e-.
        </p>
        <p>
            A redução ocorre quando uma espécie ganha elétrons e se torna eletricamente neutra: B+ + e- → B.
        </p>
        <p>
            Nas equações químicas, essa transferência de elétrons é demonstrada pela mudança do número de oxidação (nox).
        </p>
        <p>
            As reações de oxirredução ocorrem no interior das pilhas e a corrente elétrica surge com a migração dos elétrons do polo negativo para o positivo.
        </p>

        <h4>Diferença entre pilhas e baterias</h4>
        <img class="pilhaebateria" src="../imgs/desenhopilhaebateria.png" alt="pilhaebateria">
        <p>
            As pilhas e as baterias são dispositivos estudados em Eletroquímica que transformam energia química em energia elétrica. Dentro desses aparelhos ocorrem reações de oxirredução, em que há transferência de elétrons, produzindo assim corrente elétrica.
        </p>
        <p>
            A diferença entre as pilhas e baterias está no fato de que as pilhas, também chamadas de células eletroquímicas, são formadas por dois eletrodos (positivo (cátodo) e negativo (ânodo)) onde ocorrem respectivamente as semirreações de redução e oxidação, além de um eletrólito, que é uma solução condutora de íons.
        </p>
        <p>
            Já as baterias são formadas por várias pilhas ligadas em série ou em paralelo. Graças a isso, as baterias produzem uma corrente elétrica muito mais forte que as pilhas.
        </p>
        <p>
            Além disso, as pilhas e baterias podem ser divididas em primárias (não recarregáveis) e secundárias (recarregáveis).
        </p>

        <h5>Como determinar a resistência interna de uma fonte de tensão real</h5>
        <p>
            A fonte real de tensão é um elemento que fornece energia ao circuito através de uma tensão em seus terminais, sendo o valor desta tensão dependente do valor ou da direção da corrente que ela fornece. Por esta razão ela não pode fornecer qualquer potência que o circuito demandar.  Na prática pode-se pensar que dentro dela existe uma resistência muito baixa e que, quando a carga for comparável com essa resistência, parte significativa dessa energia é dissipada pela resistência interna.
        </p>
        <p>
            No diagrama de circuito fornecido, uma fonte de tensão genuína pode ser observada. Essa fonte de tensão é composta por uma fonte de tensão ideal, denotada por E, que é conectada em série com sua resistência interna, representada por r.
        </p>
        <img class="img1" src="../imgs/img1.png" alt="img1">
        <p>
            Para determinar a tensão interna da fonte (E), um método simples envolve medir diretamente a fonte usando um multímetro. É importante observar que esta medição deve ser feita sem nenhuma carga conectada à fonte.
        </p>
        <img class="img2" src="../imgs/img2.png" alt="img2">
        <p>
            Ao conectar uma carga (R) à fonte fornecida, ocorrerá um fluxo de corrente (I) resultando na geração de uma queda de tensão interna (Vr) e externa (VR).
        </p>
        <img class="img3" src="../imgs/img3.png" alt="img3">
        <p>
            Utilizando a equação para divisão de tensão, podemos derivar a seguinte expressão:
        </p>
        <img class="eq1" src="../imgs/eq1.png" alt="eq1">
        <p>
            Multiplicando o termo "r+R" à esquerda e dividindo o termo "VR" à direita, obtemos a seguinte equação:
        </p>
        <img class="eq2" src="../imgs/eq2.png" alt="eq2">
        <p>
            Depois de isolar a letra "r", ficamos com:
        </p>
        <img class="eq3" src="../imgs/eq3.png" alt="eq3">
        <p>
            Enfatizando a importância da letra "R" é:
        </p>
        <img class="eq4" src="../imgs/eq4.png" alt="eq4">
        <p>
            Ao empregar este método, temos a capacidade de medir indiretamente a resistência interna de uma célula ou bateria. Isso é feito avaliando a tensão interna (sem qualquer carga) denotada como "E", a tensão externa (com carga) referida como "VR" e a resistência externa rotulada como "R". Utilizando a equação para resistência interna denotada como "r", podemos derivar as medidas desejadas.
        </p>
    </main>
@endsection

@section('rodape')
    <h4>Rodapé da página teoria</h4>
@endsection