@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Conclusões</h1>
        <hr>
        <p>
            Através da prática obtivemos os valores das resistências internas das pilhas/baterias(r), fonte de tensão real(E), a queda de tensão externa (VR) e a corrente presente em cada bateria. O valor da tensão interna "r" de uma pilha/bateria sempre será menor que o valor da tensão interna (sem carga) "E". Isso ocorre porque ao se medir "r", existe uma carga "VR" da tensão externa que ocasionará uma queda de tensão na corrente. Além de ser possível calcular se as pilhas/baterias estão ou não em seu tempo útil de vida.
        </p>
    </main>
@endsection

@section('rodape')
    <h4>Rodapé da página conclusões</h4>
@endsection